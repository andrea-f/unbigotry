# unbigotry

## Installation

    sudo pip install twisted ...
    mongorestore data/news_index_dump

## Running

- Install the chrome extension in the folder `chrome-extension`
- `$ cd python-server`
- `python webserver.py`
