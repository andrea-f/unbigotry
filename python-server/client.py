import httplib2
from datetime import datetime
import simplejson


sample = {
            'url': "www.theguardian.co.uk",
            'content': 'This is an article.....'
        }
URL = 'http://localhost:8080/form'

jsondata = simplejson.dumps(sample)
h = httplib2.Http()
resp, content = h.request(URL,
                          'POST',
                          jsondata,
                          headers={'Content-Type': 'application/json'})
print resp
print content