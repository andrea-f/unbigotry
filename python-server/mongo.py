from pymongo import Connection
import os, sys
from os import listdir
from os.path import isfile, join


class MongoDB():
	
	def __init__(self,):
		connection = Connection()
		self.db = connection['unbigotry-database']
		self.collection = self.db['unbigotry-collection']
	


	def readFolder(self,dataDir = None):
		if dataDir is None:
			dataDir = self.dataDir
		fs = []
		for dirname, dirnames, filenames in os.walk(dataDir):
			for subdirname in dirnames:
				d = os.path.join(dirname, subdirname)
				for f in os.listdir(d):	fs.append(d+"/"+f)
			for filename in filenames:
				fs.append(os.path.join(dirname, filename))
		#fs = [ f for f in listdir(dataDir) if isfile(join(dataDir,f)) ]
		return fs



	def addToMongo(self, fs ):
		"""
		entry = {
			"label":"conservative",
			"article":"this is a conservative article about some stuff that conservatives do and write about possibly wrong traditions?"
		}
		"""
		entries = self.db.entries
		added = 0
		for fname in fs:
			with open(fname) as f:
				content = f.readlines()
				article_url = content[0]
				article_text = ' '.join(content[1:])
				entry = {
					"url": article_url,
					"text": article_text
				}
				added += 1
				entry_id = entries.insert(entry)
				print "Added: %s" % entry['url']
		
		print "Added %s records to DB" % added
if __name__ == '__main__':
	try:
		dataDir = sys.argv[1]
	except:
		dataDir = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'data', 'papers'))
	print dataDir
	mg = MongoDB()
	fs = mg.readFolder(dataDir)
	mg.addToMongo(fs)
