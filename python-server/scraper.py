import feedparser
import os,sys
from pprint import pprint
import cookielib, urllib2
from bs4 import BeautifulSoup
feeds = {
    'nytimes':[
      'http://www.nytimes.com/roomfordebate/topics/politics.rss',
      'http://www.nytimes.com/services/xml/rss/nyt/World.xml',
      'http://rss.nytimes.com/services/xml/rss/nyt/US.xml'
      ],
    'guardian':[
        'http://www.theguardian.com/world/rss',
        'http://www.theguardian.com/us/rss',
        'http://www.theguardian.com/technology/politics/rss'
    ]}
for feednom in feeds.keys():
  print 'Scraping data from %s'%feednom
  fctr=0
  for feedurl in feeds[feednom]:
    print 'Using RSS feed %s'%feedurl
    
    #New York Times class=
    classNameContent=""
    tagForMyContent=""
    if "nytimes" in feedurl:
        classNameContent="story-body-text story-content"
        tagForMyContent="p"
    
    
    if "guardian" in feedurl:
        classNameContent="content__article-body from-content-api js-article__body"
        tagForMyContent="div"
    
    
        
    print "Feed url: %s" % feedurl
    feed = feedparser.parse(feedurl)
    for entry in feed['entries']:
      with open('../data/papers/%s/%s_%04d.txt'%(feednom,feednom,fctr),'wb') as f:
        print 'Saved file number %d in %s'%(fctr,feednom)
        #Write the url at the top of the file
        f.write((entry['id']+'\n').encode('utf-8'))
        cj = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        request = urllib2.Request(entry['link'])
        response = opener.open(request).read()
        soup = BeautifulSoup(response)
        bodyText=soup.find_all(tagForMyContent,classNameContent)
        for text in bodyText:
            textRaw=text.get_text()
            f.write((textRaw+'\n').encode('utf-8'))
        fctr=fctr+1
