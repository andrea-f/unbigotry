from twisted.web import server, resource
from twisted.internet import reactor, endpoints
from pprint import pprint
import pickle
import simplejson
import urllib2
from freqcalc import compare_sources, bias_measure
from similarity import get_similar

#tfscores will be loaded from database at run-time really...
#document1=unicode(open('../data/answers/scraped/deduped_conservative.txt').read(),'utf-8')
#document2=unicode(open('../data/answers/scraped/deduped_libdem.txt').read(),'utf-8')
#tfscores = compare_sources([[document1],[document2]])
#for tfs in tfscores:
#  print tfs[:10]
tfscores=pickle.load(open('../data/conservative_libdem_tfidf_scores.pickle'))

class UnBigotry(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def render_GET(self, request):
        self.numberRequests += 1
        request.setHeader("content-type", "text/plain")
        return "I am request #" + str(self.numberRequests) + "\n"

    def render_POST(self, request):
        #request.setHeader("content-type", "application/json")
        request.responseHeaders.addRawHeader(b"content-type", b"application/json")
        content = urllib2.unquote(request.content.getvalue()[len('content='):].replace('+',' ')).decode('utf8')
        political_bias = bias_measure(content,tfscores)
        similar_articles = get_similar(content)

        info = {
            'political bias': {
              'conservative': political_bias[0],
              'liberal democrat': political_bias[1]
            },
            'similar documents': similar_articles
        }
        print info
        jsondata = simplejson.dumps(info)
        return jsondata

endpoints.serverFromString(reactor, "tcp:8080").listen(server.Site(UnBigotry()))
print('Started webserver on port 8080')
reactor.run()
