import math
from textblob import TextBlob as tb

stopwords=set('''a
about
above
after
again
against
all
am
an
and
any
are
aren't
as
at
be
because
been
before
being
below
between
both
but
by
can't
cannot
could
couldn't
did
didn't
do
does
doesn't
doing
don't
down
during
each
few
for
from
further
had
hadn't
has
hasn't
have
haven't
having
he
he'd
he'll
he's
her
here
here's
hers
herself
him
himself
his
how
how's
i
i'd
i'll
i'm
i've
if
in
into
is
isn't
it
it's
its
itself
let's
me
more
most
mustn't
my
myself
no
nor
not
of
off
on
once
only
or
other
ought
our
ours	ourselves
out
over
own
same
shan't
she
she'd
she'll
she's
should
shouldn't
so
some
such
than
that
that's
the
their
theirs
them
themselves
then
there
there's
these
they
they'd
they'll
they're
they've
this
those
through
to
too
under
until
up
very
was
wasn't
we
we'd
we'll
we're
we've
were
weren't
what
what's
when
when's
where
where's
which
while
who
who's
whom
why
why's
with
won't
would
wouldn't
you
you'd
you'll
you're
you've
your
yours
yourself
yourselves
'''.split('\n'))




#Note that this seems to break on non-ascii characters

def table(a):
  """Counts the number of elements in a in an efficient way."""
  d={}
  for i in a:
    d[i]=d.get(i,0)+1
  return d

def tf(word, blob_ctr,nblobs):
    #Hacky backup
    if word in blob_ctr:
      return blob_ctr[word]*1.0 / nblobs
    else:
      return 0.0

def n_containing(word, bloblist):
    return sum(1.0 for blob in bloblist if word in blob)

def idf(word, bloblist):
    tmp=n_containing(word,bloblist)
    if tmp==0:
      return 0.0
    return math.log(len(bloblist) / (1.0*n_containing(word, bloblist)))

def tfidf(word, blob_ctr,nblobs, bloblist):
    return tf(word, blob_ctr,nblobs) * idf(word, bloblist)


document1 = """Python is a 2000 made-for-TV horror movie directed by Richard
Clabaugh. The film features several cult favorite actors, including William
Zabka of The Karate Kid fame, Wil Wheaton, Casper Van Dien, Jenny McCarthy,
Keith Coogan, Robert Englund (best known for his role as Freddy Krueger in the
A Nightmare on Elm Street series of films), Dana Barron, David Bowe, and Sean
Whalen. The film concerns a genetically engineered snake, a python, that
escapes and unleashes itself on a small town. It includes the classic final
girl scenario evident in films like Friday the 13th. It was filmed in Los Angeles,
 California and Malibu, California. Python was followed by two sequels: Python
 II (2002) and Boa vs. Python (2004), both also made-for-TV films."""

document2 = """Python, from the Greek word , is a genus of
nonvenomous pythons[2] found in Africa and Asia. Currently, 7 species are
recognised.[2] A member of this genus, P. reticulatus, is among the longest
snakes known."""


def single_source(source,bg):
  '''
  Calculates tfidf for document 'source' relative
  to the documents in the list bg
  '''
  source=tb(source.lower())
  bg=[x.lower() for x in bg]
  ctrs=table(source.words)
  cnts=len(source.words)
  #Get a list of the words to look for tfidf on
  words=list(set([x for x in source.words if x not in stopwords]))
  scores={word:tfidf(word,ctrs,cnts,bg) for word in words}
  sorted_words=sorted(scores.items(),key=lambda x:x[1],reverse=True)
  return sorted_words


#Calculate the common words for a collection of articles
def compare_sources(collections):
  '''
  collections is a list of lists of documents
  Find the words that are most representative of the
  different sources using tfidf
  '''
  #Convert everything to a textblob object
  collections = [tb((u''.join(x)).lower()) for x in collections]
  blob_ctrs = [table(x.words) for x in collections]
  blob_cnts = [len(x.words) for x in collections]
  #Get a big list of words used by all
  words=list(set([x for x in sum([y.words for y in collections],[]) if x.lower() not in stopwords]))
  scores = [{word: tfidf(word,blob_ctrs[i],blob_cnts[i],collections) for word in words} for i in xrange(len(collections))]
  sorted_words=[sorted(scores[i].items(), key=lambda x:x[1],reverse=True) for i in xrange(len(scores))]
  ##Convert everything to a textblob object
  #blobsa=tb(''.join(ca))
  #blobsb=tb(''.join(cb))
  #bloblist=[blobsa,blobsb]
  ##What are the words that are used in both?
  #words=list(set([x.lower() for x in (blobsa.words+blobsb.words) if x.lower() not in stopwords]))
  ##Search through each article and sort them by tfidf score
  #scoresa = {word: tfidf(word,blobsa,bloblist) for word in words}
  #scoresb = {word: tfidf(word,blobsb,bloblist) for word in words}
  #sorted_wordsa = sorted(scoresa.items(), key=lambda x:x[1],reverse=True)
  #sorted_wordsb = sorted(scoresb.items(), key=lambda x:x[1],reverse=True)
  return sorted_words

def bias_measure(document,classes):
  '''
  Returns the percentage bias of two extremes of a given
  document, based on some collection of tfidf terms termsa,termsb
  '''
  document=tb(document.lower())
  nterms=len(document.words)
  ret=[]
  for clazz in classes:
    num=0.0
    ctr=table([_ for _ in document.words])
    for term,tfscore in clazz:
      if term in ctr:
        num=num+1.0*ctr[term]*tfscore/nterms*1.0
    ret.append(num)
  return ret



