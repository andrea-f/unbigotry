from textblob import TextBlob as tb
from freqcalc import single_source
from pymongo import MongoClient, ASCENDING


mongo_client = MongoClient()

db = mongo_client['unbigotry-database']
news = db.entries
biases = db.biases
index = db.news_index
index.create_index([('term', ASCENDING)])

bg_data = [doc['text'] for doc in news.find()]

def put_prob(term, id, prob):
  existing = index.find_one({'term': term})
  if existing:
    index.update({'term': term}, {'$push': {'sources': [id, prob]}})
  else:
    index.insert({'term': term, 'sources': [[id, prob]]})

def index_doc(doc, id):
  term_freqs = single_source(doc, bg_data)
  for (term, prob) in term_freqs:
    put_prob(term, id, prob)

def get_similar(doc, n=10, n_terms_to_compare=100000):
  term_probs = single_source(doc, bg_data)[:n_terms_to_compare]
  results = {}
  for (term, term_prob) in term_probs:
    index_entry = index.find_one({'term': term})
    if index_entry:
      for (id, doc_prob) in index_entry['sources']:
        results[id] = max(results.get(id, 0.0), doc_prob)
  ret = sorted(results.items(), key=lambda x:x[1], reverse=True)[:n]
  return [(x[0],x[1],db.biases.find_one({'url':x[0]})['bias']) for x in ret]

def delete_items(db):
  'Use with caution!'
  db.remove({})

def index_db(db):
  i = 0
  for doc in news.find():
    print i
    index_doc(doc['text'], doc['url'])
    i += 1

if __name__ == '__main__':
  print('Removing existing index...')
  delete_items(index)

  print('Indexing news...')
  index_db(news)

  art = news.find()[1]
  print repr(art['url']), '~=', get_similar(art['text'])
