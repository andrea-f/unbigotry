import sys
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

if __name__ =="__main__":
    with open(sys.argv[1], "r") as file:
        with open(sys.argv[2], "w") as new_file:
            new_file.write(strip_tags(file.read()))
