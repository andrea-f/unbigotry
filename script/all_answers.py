from __future__ import print_function
import json
import urllib2
import sys
import os

files = {}


def openAllFiles():
	for file in os.listdir("../data/members"):
		#TODO: use regex
		party = file.replace("../data/members/", "")
		party = party.replace(".txt", "")
		files[party] = open("../data/answers/" + party + ".txt", "w")

def closeAllFiles():
	for key in files:
		files[key].close()

def clean(string):
	return ''.join(string.split())

def whichParty(member):
	return members.get(clean(member))

if __name__ == "__main__":
	openAllFiles()

	#get members dictionary
	with open('../data/members.json', "r") as mem_file:
		members = json.load(mem_file)

	next = 'http://lda.data.parliament.uk/answeredquestions.json'
	i = 0
	while next:
		jstr = urllib2.urlopen(next).read()
		ts = json.loads( jstr )
		for item in ts["result"]["items"]:
			answer = item["answer"]
			#TODO: there's other HTML elements to remove
			text = answer["answerText"].replace("<p>", "")
			text = text.replace("</p>", "").encode('utf8') + "\n"
			member = answer["answeringMember"].replace("http://data.parliament.uk/members/", "").encode('utf8')
			party = whichParty(member)
			if party:
				files[party].write(text)
			i += 1
			print(i)
		next = ts["result"].get("next")
	closeAllFiles()