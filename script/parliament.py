import json
import urllib2
import sys


apiUrl='http://lda.data.parliament.uk/commonsmembers.json?' # set the query word here
apiParty='party=' + sys.argv[1]                     # set the date here
apiPage='_page=0'      # set the page          # set the number of articles in one page

link=[apiUrl, apiParty, apiPage]
ReqUrl='&'.join(link)


ids = []
next = ReqUrl
while next:
	jstr = urllib2.urlopen(next).read()  # t = jstr.strip('()')
	ts = json.loads( jstr )
	for member in ts["result"]["items"]:
		id = member["_about"].replace("http://data.parliament.uk/members/", "")
		ids += [id]
		print id
	next = ts["result"].get("next")
