import json
import urllib2
import sys



def whichParty(member, con, lab):
	if isParty(con, member):
		return "con"
	if isParty(lab, member):
		return "lab"
	return None


def isParty(mem_file, mem):
	for line in mem_file:
		if clean(line) == clean(mem):
			return True
	return False

def clean(string):
	return ''.join(string.split())


con_answers = open("../data/con_answers.txt", 'w')
lab_answers = open("../data/lab_answers.txt", 'w')
con = open('../data/members/Conservative.txt', "r")
lab = open('../data/members/Labour.txt', "r")

answers = []
next = 'http://lda.data.parliament.uk/answeredquestions.json'
i = 0
while next and i <=40:
	jstr = urllib2.urlopen(next).read()
	ts = json.loads( jstr )
	for item in ts["result"]["items"]:
		answer = item["answer"]
		#TODO: there's other HTML elements to remove
		text = answer["answerText"].replace("<p>", "")
		text = text.replace("</p>", "").encode('utf8') + "\n"
		member = answer["answeringMember"].replace("http://data.parliament.uk/members/", "")
		party = whichParty(member, con, lab)
		if party == "con":
			con_answers.write(text)
		elif party == "lab":
			lab_answers.write(text)
		i += 1

	next = ts["result"].get("next")
con_answers.close()
lab_answers.close()
con.close()
lab.close()
	


	

