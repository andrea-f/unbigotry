var exampleResponse = {
  'political bias': {
    'left wing': 0.1,
    'right wing': 0.7
  }
};

function getArticleContent() {
  return $('p').text();
}

function rightwingness(left, right) {
  console.log(left, right)
  return Math.log(right / left);
}

function showBadge(response) {

  var political_lean = rightwingness(response['political bias']['liberal democrat'], response['political bias']['conservative']);

  $('<div id="unbigotry-badge" class="source">')
    .append(
      $('<span class="unbigotry-close">')
        .text('x')
        .click(function(e) {
          $(e.target).closest('#unbigotry-badge').hide();
          e.preventDefault();
        }),
      $('<h3>')
        .text('This article'),
      $('<h4>')
        .text('Political bias')
    )
    .append(
      $.map(response['political bias'], function(prob, label) {
        return $('<span class="label">')
          .addClass(label)
          .text(label + ': ' + prob);
      })
    )
    .append(
      $('<span>').text( 'Right-wingness: ' + political_lean )
    )
    .hide()
    .appendTo('body')
    .fadeIn();

  $('<div id="unbigotry-related">')
    .append(
      $('<h3>').text('Related articles'),
      $('<span class="unbigotry-close">')
        .text('x')
        .click(function(e) {
          $(e.target).closest('#unbigotry-related').hide();
          $('body').css('padding-bottom', 0);
          e.preventDefault();
        })
    )
    .append(
      $.map(response['similar documents'], function(item) {
        var bias = rightwingness(item[2][1], item[2][0]);
        return [
          $('<a>').attr('href', item[0]).text(item[0] + '(rightwingness: ' + bias + ', similarity: ' + item[1] + ')')
        ];
      })
    )
    .appendTo('body');
}


$(function() {

  var content = getArticleContent();
  if (content.length > 100) {
    $.post(
      'http://localhost:8080',
      {
        /*url: window.location.href,*/
        content: content
      },
      function(response) {
        console.log(response);
        showBadge(response);
      }
    );
  }

});
