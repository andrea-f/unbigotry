http://www.nytimes.com/2015/02/08/nyregion/the-hidden-money-buying-up-new-york-real-estate.html
For more than a year, The New York Times examined the influx of global cash fueling the city’s high-end real estate boom. The investigation pierced the secrecy of more than 200 shell companies that have owned condominiums at one complex, the Time Warner Center.
The Times found:
■ Nearly half of the most expensive residential properties in the United States are now purchased anonymously through shell companies.
■ The real estate industry does little examination of buyers’ identities or backgrounds, and there is no legal requirement for it to do so.
■ At the Time Warner Center, 37 percent of the condominiums are owned by foreigners. At least 16 foreigners who have owned in the building have been the subject of government inquiries, either personally or as heads of companies.
Read the first part of the series here. Four more stories will be published in the coming week.
The Times traced condominiums to people from all over the world and many different industries. Here are some people who have been tied either personally or through close relatives to Time Warner:
