http://www.theguardian.com/media/2015/feb/07/nbc-anchor-brian-williams-off-air-iraq-katrina

Brian Williams is temporarily stepping away from NBC Nightly News while the network continues its investigation into inaccuracies in his reporting, he announced on Saturday.
Williams has come under pressure after veterans questioned his account of an incident involving two US helicopters that came under fire in Iraq in 2003. 
His reporting from New Orleans in the aftermath of Hurricane Katrina in 2005 was subsequently called into question, partly over whether he could have seen a body “float by face down” from his “hotel room window in the French Quarter”.

 Related: Can Brian Williams' career survive the retraction of his Iraq tale? 

In a statement, Williams said: “In the midst of a career spent covering and consuming news, it has become painfully apparent to me that I am presently too much a part of the news, due to my actions.
“As managing editor of NBC Nightly News, I have decided to take myself off my daily broadcast for the next several days, and Lester Holt has kindly agreed to sit in for me to allow us to adequately deal with this issue. Upon my return, I will continue my career-long effort to be worthy of the trust of those who place their trust in us.” 
On Friday, NBC News president Deborah Turness said in an internal memo to staff: “This has been a difficult few days for all of us at NBC News. As you would expect, we have a team dedicated to gathering the facts to help us make sense of all that has transpired. We’re working on what the best next steps are.”
Turness said the network had assigned the head of its investigative unit to look into Williams’ statements.
Regarding his version of events in Iraq, Williams apologised on air on Wednesday, saying: “I made a mistake in recalling the events of 12 years ago. I want to apologise. I said I was traveling in an aircraft that was hit by RPG fire. I was instead in a following aircraft.”

