http://www.theguardian.com/society/2015/feb/07/measles-vaccination-rates-africa-surpass-north-america

While parts of North America are experiencing the worst measles outbreak in 15 years, a new report shows that Africa has increased immunisation rates significantly, making the continent a world leader in protecting children against the disease.
The widespread availability of safe and affordable vaccines in even the most volatile and poor regions of African countries has seen immunisation rates surpass those of in the US, according to the 2015 Africa Survey, an annual report by Good Governance Africa (GGA).
The US has a 91% vaccination rate, while in Canada, which is currently experiencing an outbreak in Toronto, it is 84%, according to a UN estimate. A 95% rate is required for so-called “herd immunity”.
A survey this week by Canadian researchers found that a fifth of the population still believed the long-debunked myth that the measles vaccine causes autism.
Ninety per cent of measles cases in the US involve people who have not been vaccinated or whose vaccination status is unknown, according to US Centers for Disease Control.
The GGA survey indicates that some African countries are now achieving near 100% vaccination rates, up by 39% since 2000.
GGA, a research and advocacy organisation based in South Africa, produces a collection of social, political and economic indicators from all 55 African countries, including information on the fight against measles from World Health Organisation (WHO).
The survey shows that 16 countries in Africa, including Tanzania, Morocco, Libya, Mauritius, Eritrea, Gambia and Egypt have almost 100% vaccination rates, and five others - Zimbabwe, Algeria, Kenya, Botswana and Lesotho, have higher rates than the US.
This recent progress is part of Africa’s bigger measles success story.
In 2000, WHO reported that 60% of the 777,000 measles deaths worldwide occurred in occurred in sub-Saharan Africa. With improved communication, social mobilisation, counselling and funding, however, measles deaths were reduced by 91% by 2007. At the time, WHO’s director general of health, Margaret Chan, described the trend as “a major public health success and a tribute to the dedication of the countries in the African region.”
That said, in 2000 an estimated 600 African children died from measles every day. Despite improved vaccination rates, the figure still stands at 400 a day, according to GGA’s researcher, Kate Van Niekerk.
Africa still suffers with poor health systems as demonstrated by the Ebola outbreak, and the UN reports that most countries in sub-Saharan Africa will not meet the millennium development goals, including those on reducing child and maternal mortality.
According to Van Niekerk, however, Africa’s success against measles shows that vaccination can be successful in reducing child mortality in even the poorest of communities.

