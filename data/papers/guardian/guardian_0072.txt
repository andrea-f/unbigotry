http://www.theguardian.com/global-development/2015/feb/04/eritrea-migration-chance-reunion-calais-human-rights

On a cold January day, Feruz Werede travelled from her home in London to Calais to learn more about the thousands of young people who are risking their lives to flee the country of her birth, Eritrea.



 Facebook 
 Twitter 
 Pinterest 
  expand 


Feruz Werede


Feruz, an activist who campaigns to end rights abuses in Eritrea, wanted to interview migrants for a radio station that broadcasts by satellite to the Horn of Africa country. She expected to hear about the “never-ending national service” and to collect material for a Stop Slavery in Eritrea campaign against conscription.
What she did not expect was a surreal, early-morning reunion with a high-school classmate. It was a chance meeting that highlighted her own good fortune and the scale of the exodus from a country once viewed as a poster child for liberty but now dubbed “the North Korea of Africa” because of its repressive one-party rule, arbitrary detentions and killings, and lack of an independent press.
A former Italian colony, Eritrea gained independence from Ethiopia in 1993 after a 30-year guerrilla war. But fighting broke out again between the two countries in 1998-2000. Estimates of the number of lives lost in the conflict vary from 80,000 to 100,000. Since then, Eritrea has existed in a precarious state of “no peace, no war” beside its bigger neighbour. It is this relationship that defines President Isaias Afewerki’s rule.
Feruz’s parents were fighters during the war against Ethiopia. After independence, they joined the government but became critical of the authorities and eventually had to flee. Feruz left with her mother and arrived in the UK on Christmas Eve 2001. She was granted asylum, finished her studies and built a new life. But she has never forgotten her home country and spends her free time campaigning to bring about change.
On her recent visit to Calais, Feruz arrived on a freezing Sunday evening but was told that most migrants in the sprawl of tents known as the jungle were getting ready for their nightly attempts to sneak on to lorries to cross the the Channel. She returned the next morning at 7am, but people were sleeping, exhausted after another cold night watching and waiting for an elusive chance to complete the last leg of their complex, life-defining journeys.



 Facebook 
 Twitter 
 Pinterest 
  expand 


Migrants, predominantly from Eritrea, shelter in an abandoned warehouse in Calais.
Photograph: Etienne Laurent/EPA


Eventually, Feruz was approached by a young man, who recognised her as Eritrean and addressed her in Tigrinya. She described the encounter: “He said, ‘I saw you earlier. How are you?’ We exchanged pleasantries, and we started talking. He could tell I was from Asmara, the capital city, so he started asking me, ‘Which part of the city did you live in? Is it Geza-Manda or Gejeret?’ I said, ‘It’s Geza-Manda, but it’s been years and I can’t remember exactly where.’
“While he was talking, he mentioned my high school, and I just stood there and said, ‘You went to Santa Ana? I can’t believe it. Which year did you go?’ He said from 1996 to 2000. And I said, ‘But I was there from that time!’ It ends up that we were in the same class in year nine. 
“We went back and forth about our lives in high school and he started mentioning all the people that I know. He was telling me, ‘This person is my friend,’ and I said, ‘But he’s my friend, I still have contact.’ I took out my phone and I started showing him on Facebook.”
Feruz’s new-old friend joked that she had been “one of the cool girls” in school and had never paid any attention to him and his friends. 
“We spent hours talking,” Feruz said. “He told me so many people’s names, how they died crossing the border, the people we saw every day, about how some of them didn’t make it in the Mediterranean sea, even including Lampedusa, because we lost some of our schoolmates there.”



 Facebook 
 Twitter 
 Pinterest 
  expand 


One of Feruz’s former classmates rests in his tent in Calais. 
Photograph: Feruz Werede


Eritreans make up a large proportion of the illegal migrants arriving in southern Europe each year. The UN refugee agency, UNHCR, says the number of Eritrean asylum seekers rose threefold to nearly 37,000 over the first 10 months of 2014. Last October, the UN’s special rapporteur on human rights in Eritrea said human rights abuses were forcing 2,000-3,000 people to flee each month.
Those alleged abuses include extrajudicial killings, disappearances, extended incommunicado detention, torture, indefinite national service, and lack of freedom of expression, assembly, religious belief and movement, the UN said.
It is the indefinite national service, or what she calls slavery, that most concerns Feruz, and it is why her two former classmates left Eritrea.
“Imagine, our generation in Eritrea – it is vanishing,” Feruz said. “If these kinds of stories can come out of one class, what can come out of one neighbourhood? What could come out of one town, one village and ultimately the entire country? My class consisted of about 40-something students … A few of the girls who were with me are married and have kids in Eritrea, but most left, or died trying to leave.”
As Feruz chatted to her classmate, another man walked up. “How are you, Feruz?” he asked. He too had been in her class, and had recognised her. As the men told Feruz why they had left, she could not help but compare her story with theirs. 
“I had mixed feelings,” Feruz said. “Obviously, I did feel privileged. I finished university here [in the UK], I achieved something, I’m working, I have a life, I have children. Whereas my friends in Calais, even if they could settle right this moment, they will still feel they haven’t achieved what they should have achieved in their 30s. 
“It’s quite depressing and liberating at the same time, finding someone 15-16 years later. They probably had the same aspirations as I had, and to see them in Calais, it just broke me. I was emotional for a few days afterwards.
“All this time that I was here making my life comfortable, they had been in the mountains of Eritrea, defending a mountain, basically. One of them told me he was found trying to escape. He was caught at the border and put in prison for four years. It’s the never-ending national service which is straight-out slavery in my eyes, and in their eyes: not being able to support your family, not being able to live. There are no prospects for them, and they tolerated it for eight, nine years but they can’t, they just can’t.”
All Eritreans aged between 18 and 40 are obliged to do national service, which is meant to last 18 months. In practice, the term tends to be indefinite. According to a report presented to the UN by the special rapporteur last May, conscripts can be ordered to work on farms or construction sites as part of the government’s “development programmes”. Trying to dodge national service is perilous: the military police routinely carry out roundups, known as giffas, to catch anyone, including minors, escaping from conscription. “Opposing such a roundup can lead to on-the-spot execution, as deadly force is permitted against those resisting or attempting to flee,” the report said.



Feruz’s class at her school in Asmara.
Photograph: Feruz Werede


Life as a conscript is harsh. “The salary and allowances for dependants during the national service is so low that recruits are unable to fend for themselves or support their families,” the report said. “Living conditions are harsh, food rations are inadequate, there is a lack of medicines and trained medical personnel, while the paltry remuneration stops many young people from even contemplating marriage, and a long-term future.”
The report concluded: “Since the length of national service in Eritrea is of an indefinite nature, it effectively constitutes forced labour.” 
This is what Feruz’s friends were fleeing. Once out of Eritrea, they travelled through Sudan, heading for Libya and the often deadly boats that ferry thousands of migrants from the shores of Africa. Even if they do survive the callous crews and the dangerous waves, they face new challenges as they seek to rebuild their lives and become something other than “refugees”.



 Facebook 
 Twitter 
 Pinterest 
  expand 


Migrants gather food and supplies during the day, waiting for the night when they will try to sneak on to trucks heading to England from Calais.
Photograph: Etienne Laurent/EPA


“We Eritreans almost feel like we have no identity at the moment. Every single ounce of our pride, which is our country – and we are very patriotic people – is being stripped away,” Feruz explained. 
“When you leave the country, you are automatically a traitor to the regime, and you are an asylum seeker in your next destination. To be somewhere in between – can you imagine? – you have no identity. These people feel they have no identity, they have to seek that identity, and that identity is across the English Channel for them.”
Despite her own history as an asylum seeker more than a decade ago, Feruz was appalled by living conditions in Calais’s jungle. 
“It was shocking. I couldn’t speak for half an hour. But when you speak to them, it’s almost normal. That’s the first vibe you get because these people have seen the Sahara desert, the Mediterranean. They’ve been chased by human traffickers who want to sell them to the Bedouin. They have seen the most horrific ordeals. Now, they are relatively safe because they are not bothered by people wanting to use them. The only thing that is difficult for them at the moment is the cold weather and, obviously, they are not settled. Apart from that, they’ve moved on from the worst things. The fact that they are still chasing their dream makes it worthwhile.
“I spoke to one guy and I said, ‘How do you guys live here?’ He said, ‘Oh but Feruz, you come from a warm bed, you come from a warm house, you can eat any time. Of course, this is going to shock you. For us, where we come from, this is something we are used to.’ In a way, they have to be very positive to survive day by day. They have to crack jokes, they have to be able to laugh and bring that positive vibe.
“When you speak to them in the night, they keep looking at the port, towards the ferries, and they say, ‘Our dream is just 43 minutes away by train.’ For someone who has come this close, they will keep trying.”



 Facebook 
 Twitter 
 Pinterest 
  expand 


A migrant tries to jump on to a lorry heading to the ferry terminal at Calais. 
Photograph: Pascal Rossignol/Reuters


The British government often asserts that immigrants are drawn to the UK because of unemployment and housing benefits. Although migrants settle in many European countries, the concentration of people in Calais suggests Britain holds a special attraction. Some migrants cite the language, some their perception of a free society, while others are drawn by the relatively strong economy and the prospects for employment. “There are opportunities [in the UK],” Feruz said. “You can take opportunities and become somebody. But although I had a vision of finishing my education and supporting myself, when I first got here and walked into the Home Office – the feeling I felt when asking for asylum …” Her voice trailed off.
“I know how unwelcoming the country can be, but they [other migrants] don’t. They want to go to a country where there are better opportunities, and they want to work and support their hungry families back home. Benefits and housing and this and that, whatever this country can offer – they don’t even know about that.
“I called them [her classmates] a few days ago, and I said, ‘Did you make it?’ and they said, ‘No, no, we are still here.’ I said I was calling from work, and they said, ‘Oh you’re so lucky, so how much do they pay you? What’s the minimum wage?’ They want to know about the minimum wage! That means they want to work.”
Feruz believes that governments can and should do more to address this growing crisis, but they should start at the source of the problem, instead of seeking to deal with the symptoms.
“I always feel the world can do more,” Feruz said. “Not necessarily the UK or France or any particular European country, but the world can do more to solve issues for any migrants who are trying to flee dictatorship. One is to listen to the people and try to find the root cause of the issue. The actual disease has to be addressed and that is the dictatorship back home that is pushing every single able-bodied person out of the country.”
In a debate in the House of Lords in January, Baroness Eluned Morgan, the Labour party spokeswoman on foreign affairs in the upper house, described the situation in Eritrea as “truly catastrophic”.
“This is what people don’t understand,” Feruz said, “and it took me going to Calais to understand: for us, it’s very difficult to comprehend someone staying out there in the cold weather, but for them, if it means, even a year later, getting to where they plan to get to, that’s nothing.”

