http://www.theguardian.com/world/2015/feb/06/uruguay-expels-iranian-diplomat-fake-bomb-israeli-embassy

Tehran’s ambassador to Uruguay was summoned to the country’s foreign ministry to explain why an Iranian diplomat was spotted close to a dummy bomb found near the Israeli embassy in Montevideo.
On 24 November, Montevideo bomb squad officers detonated what turned out to be a fake bomb near the Israeli embassy, located in the World Trade Centre office complex in the city.
The convincing-looking fake – complete with fuse, detonator and other elements found in a real bomb – was detected some 70 metres (230ft) from the building by bomb-sniffing dogs.
On Friday, a joint statement released by Uruguay’s interior and foreign ministries said that when investigators reviewed CCTV footage, a car with Iranian diplomatic plates was spotted close to where the briefcase containing the dummy bomb was planted.
Although there was no evidence linking the diplomat to the briefcase “the situation was extremely worrying”, the statement adds.
On 10 December the Iranian ambassador was summoned to the Uruguayan foreign office, where he was told that “the coincidence of the presence of the Iranian official just a few dozen metres from the briefcase was unfortunate and inadmissible”, the statement said.
The ambassador said that the diplomat had been visiting his doctor and had nothing to do with the fake bomb, but was warned that Uruguay would adopt “more severe measures should similar circumstances arise in the future”, the statement said.
Citing an unidentified “senior official in Jerusalem”, the Israeli newspaper Haaretz reported that the diplomat was expelled two weeks ago after Uruguay’s intelligence services linked the fake bomb to an embassy employee.
But Uruguay’s interior ministry said the Iranian diplomat had already left the country in December, at the end of his tour of duty.
After destroying the device, bomb brigade lieutenant colonel Alfredo Larramendi told reporters that it “never posed any danger” but might have been part of a dress rehearsal for the real thing.
“It might have been put there to see the response time” of responders, or to “size up the quality of the security of Israel’s embassy”, Larramendi said.

 Related: Spies, cover-ups and the mysterious death of an Argentinian prosecutor 

Mossad agents travelled from Buenos Aires to Montevideo last week to discuss the case with Uruguayan authorities, according to the Montevideo daily el País.Uruguay’s president, José Mujica, refused to comment on the case. “I am not going to make any statement,” he told reporters on Friday morning.
Israel has long accused Iran of sponsoring attacks against it around the world, using Lebanon’s Shia Hezbollah and Palestinian Islamist group Hamas as proxies.
Uruguay’s southern neighbour Argentina has been rocked by allegations that the country’s president, Cristina Fernández de Kirchner had participated in a conspiracy to cover up Iranian involvement in the country’s worst terror attack.
Last month, prosecutor Alberto Nisman was found dead in his apartment with a gunshot wound to the head, the night before he was due to testify before congress that Fernández and her allies had conducted secret negotiations with Iran to reach a preferential trade deal in return for suppressing arrest warrants against Iranian suspects in the 1994 bombing of a Buenos Aires Jewish centre.
The bombing killed 85 people and wounded 300, the deadliest such attack in Argentina’s history.
In 2013 Nisman had accused Iran of opening secret intelligence stations in several South American countries to plan and conduct terror attacks.




